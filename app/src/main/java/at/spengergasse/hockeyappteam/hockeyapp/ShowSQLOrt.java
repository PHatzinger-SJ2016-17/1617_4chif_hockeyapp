package at.spengergasse.hockeyappteam.hockeyapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import at.spengergasse.hockeyappteam.hockeyapp.Domain.Ort;
import at.spengergasse.hockeyappteam.hockeyapp.SQLUtil.SQLOrt;

public class ShowSQLOrt extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_sqlort);
        final SQLOrt sqlOrt = new SQLOrt(this);

        final Button button = (Button) findViewById(R.id.submit);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.submit:
                        Ort ort = new Ort();
                        TextView textView = (TextView) findViewById(R.id.bezeichung);
                        ort.setBezeichnung(textView.getText().toString());
                        textView = (TextView) findViewById(R.id.addrese);
                        ort.setAddrese(textView.getText().toString());
                        textView = (TextView) findViewById(R.id.land);
                        ort.setLand(textView.getText().toString());
                        textView = (TextView) findViewById(R.id.plz);
                        ort.setPLZ(Integer.parseInt(textView.getText().toString()));

                        sqlOrt.addOrt(ort);
                        break;
                    case R.id.reset:
                        // do
                        break;
                }

            }

        });
    }


}
