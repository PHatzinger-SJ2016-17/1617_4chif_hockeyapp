package at.spengergasse.hockeyappteam.hockeyapp.Domain;

public class Ort {

    private int ortID;
    private String bezeichnung;
    private String addrese;
    private String land;
    private int PLZ;

    public int getOrtID() {
        return ortID;
    }

    public void setOrtID(int ortID) {
        if(ortID >= 0) {
            this.ortID = ortID;
        }
    }

    public String getAddrese() {
        return addrese;
    }

    public void setAddrese(String addrese) {
        if(addrese != null && addrese != "") { //TODO Check if addrese is not biger then 3 letters
            this.addrese = addrese;
        }
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        if(bezeichnung != null && bezeichnung != "") {
            this.bezeichnung = bezeichnung;
        }
    }

    public String getLand() {
        return land;
    }

    public void setLand(String land) {
        if(land != null && land != "") {
            this.land = land;
        }

    }

    public int getPLZ() {
        return PLZ;
    }

    public void setPLZ(int PLZ) {
        if(PLZ >= 0) {
            this.PLZ = PLZ;
        }
    }
}