package at.spengergasse.hockeyappteam.hockeyapp.SQLUtil;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import at.spengergasse.hockeyappteam.hockeyapp.Domain.Ort;

public class SQLOrt extends SQLiteOpenHelper{

        private static final int DATABASE_VERSION = 2;
        private static final String DATABASE_NAME = "HOCKEYDB";
        private static final String ORT_TABLE_NAME = "ORTE";

        // Contacts Table Columns names
        private static final String KEY_ID = "ortID";
        private static final String KEY_NAME = "bezeichnung";
        private static final String KEY_ADDR = "adresse";
        private static final String KEY_LAND = "land";
        private static final String KEY_PLZ = "PLZ";


        private static final String InsertIntoORT = "INSERT INTO ORTE" + " (ortID, bezeichnung, adresse, land, plz)" + "VALUES (NULL,?,?,?,?)";
        private static final String FindObjectbyID = "SELECT * " + "FROM ORTE " + "WHERE ortID=?";
        private static final String DeleteObjectbyID = "DELETE FROM ORTE " + "WHERE ortID=?";
        private static final String SelectAll = "SELECT * FROM ORTE";
        private static final String CreateOrte = "CREATE TABLE IF NOT EXISTS "+ORT_TABLE_NAME+"(" +
                "ortID INT NOT  NULL GENERATED BY DEFAULT AS IDENTITY, " +
                "bezeichnung VARCHAR(256) NOT NULL, " +
                "adresse VARCHAR(256) NOT NULL, " +
                "land VARCHAR(3) NOT NULL, " +
                "PLZ INTEGER NOT NULL, " +
                "PRIMARY KEY (ortID)" +
                ")";


        public SQLOrt(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }



    @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CreateOrte);
        }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + ORT_TABLE_NAME);

        // Create tables again
        onCreate(db);
    }

    public void addOrt(Ort ort) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, ort.getOrtID());
        values.put(KEY_NAME, ort.getBezeichnung());
        values.put(KEY_ADDR, ort.getAddrese());
        values.put(KEY_LAND, ort.getLand());
        values.put(KEY_PLZ, ort.getPLZ());

        // Inserting Row
        db.insert(ORT_TABLE_NAME, null, values);
        db.close();
    }

    public List<Ort> getAllOrt() {
        List<Ort> ortList = new ArrayList<Ort>();
        // Select All Query
        String selectQuery = SelectAll;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Ort ort = new Ort();
                ort.setOrtID(Integer.parseInt(cursor.getString(0)));
                ort.setBezeichnung(cursor.getString(1));
                ort.setAddrese(cursor.getString(2));
                ort.setLand(cursor.getString(3));
                ort.setPLZ(Integer.parseInt(cursor.getString(4)));

                ortList.add(ort);
            } while (cursor.moveToNext());
        }

        // return contact list
        return ortList;
    }




}
